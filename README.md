# Cloudburo Curation Platform Support Home Page (Beta-Phase)

This is the support page for the [Cloudburo Curation Platform](https://cloudburo.net/docs/products.html) during its beta release phase.

> Cloudburo’s Curation Platform is a content curation solution which can be managed by the end user with minimal effort in the most efficient and intuitive manner. Aimed at startups, micro businesses, small-  enterprises as well as private users, it will allow you to establish a content presence on the Internet for your audience.

> It’s an Amazon AWS Cloud-based service which seamlessly integrates into Evernote’s One Workspace ecosystem to provide you with the best user experience. Being integrated with Evernote it offers you a superior front-end in order to manage all your curation and blogging content with an easy to use tool and workflow.


## Notification about Production Status ##

Will be communicated via Twitter [@cloudburo](https://twitter.com/cloudburo), please follow to be up-to-date about production status events.

## Raising issues, change- or feature request 

Use the Issue Tracker on Bitbucket which can be found [here](https://bitbucket.org/cloudburo/curationplatform/issues)

## Documentation of the Curation Platform

Can be found under [curation.cloudburo.net](https://curation.cloudburo.net)

## Do you want to register for beta users

Please register here for [early access](https://cloudburo.net/docs/products.html)

## Third Party Notification Status

Evernote is tweeting its status over [evernotestatus](https://twitter.com/evernotestatus).

![cloudburoCurationPlatformURL.png](https://bitbucket.org/repo/55bxBj/images/349511213-cloudburoCurationPlatformURL.png)